import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import registerServiceWorker from './registerServiceWorker';

/*ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();*/


/**
 * Build the component (object) and declare and modify "state"
 */
class Clock extends React.Component {
    constructor(props) {
        super(props);
        //set the current date to the attribute "state"
        this.state = {date: new Date()}
    }
    /**
     * What we do when the component is loaded the first time
     */
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }
    /**
     * What we do when the component is not show
     */
    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    /**
     * change the date
     */
    tick() {
        this.setState({
            date: new Date(),
        }
    );
    }

    /**
     *
     * @returns {XML}
     */
    render() {
        return (
            <div>
                <h1>Hello, world!</h1>
                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
            </div>
        );
    }
}
/**
 *
 */
ReactDOM.render(
    <Clock />,
    document.getElementById('root')
);




